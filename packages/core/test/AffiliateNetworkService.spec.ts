import {AffiliateNetworkService} from "../src/AffiliateNetworkService";
import {createAndResolve} from "./utils";


describe("AffiliateNetworkService", async function () {
    this.timeout(Infinity);

    it("add", async () => {
        let {app, service} = await createAndResolve(AffiliateNetworkService);
        await service.add({
            name: "qwe",
            userId: 1,
            postbackUrl: "postbackUrl",
            offerParams: "offerParams"
        });
        await app.close();
    });

    it("update", async () => {
        let {app, service} = await createAndResolve(AffiliateNetworkService);
        await service.update(1, {
            name: "qwe11111",
            postbackUrl: "postbackUrl",
            offerParams: null,
        });
        await app.close();
    });

});

