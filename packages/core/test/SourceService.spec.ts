import {createAndResolve} from "./utils";
import {SourceService} from "../src/SourceService";


describe("SourceService", async function () {
    this.timeout(Infinity);

    it("add", async () => {
        let {app, service} = await createAndResolve(SourceService);
        await service.add({
            name: "qwe",
            userId: 1,
            postbackUrl: "postbackUrl",
        });
        await app.close();
    });

    it("update", async () => {
        let {app, service} = await createAndResolve(SourceService);
        await service.update(1, {
            // name: "sdf",
            // postbackUrl: null,
        });
        await app.close();
    });

});

