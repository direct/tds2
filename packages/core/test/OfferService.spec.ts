import {createAndResolve} from "./utils";
import {OfferService} from "../src/OfferService";

describe("OfferService", async function () {
    this.timeout(Infinity);

    it("add", async () => {
        let {app, service} = await createAndResolve(OfferService);
        await service.add({
            name: "Offer 1",
            userId: 1,
            affiliateNetworkId: 1,
            type: "local",
            file: Buffer.from([]),
            redirectType: "js",
            redirectUrl: "s",
            preloadUrl: "1",
            actionType: "text",
            actionCampaignId: 1,
            actionContent: "actionContent",
        });
        await app.close();
    });

    it("update", async () => {
        let {app, service} = await createAndResolve(OfferService);
        await service.update(1, {
            name: "Offer 1",
            affiliateNetworkId: 1,
            // type: "action",
            // actionType: "asd",
            // file: "1",
            // redirectUrl: "null",
            // redirectUrl: "s",
            // redirectType: "q",
            // preloadUrl: "1",
            // email: "email@test.com",
            // password: "pass",
        });
        await app.close();
    });

});

