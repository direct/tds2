import {UserService} from "../src/UserService";
import {createAndResolve} from "./utils";

describe("UserService", async function () {
    this.timeout(Infinity);

    it("add", async () => {
        let {app, service} = await createAndResolve(UserService);
        await service.add({
            email: "email@test.com",
            password: "pass",
        });
        await app.close();
    });

});

