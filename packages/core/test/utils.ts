import {NestFactory} from "@nestjs/core";
import {AppModule} from "../src/AppModule";
import {Type} from "@nestjs/common/interfaces/type.interface";

export async function createAndResolve<T>(type: Type<T>) {
    let app = await NestFactory.createApplicationContext(AppModule);
    return {
        service: await app.resolve(type),
        app,
    };
}