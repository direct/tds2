import {createAndResolve} from "./utils";
import {ClickService} from "../src/ClickService";
import {Click} from "../src/Click";

describe("ClickService", async function () {
    this.timeout(Infinity);

    it("add", async () => {
        let {app, service} = await createAndResolve(ClickService);
        await service.add({
            visitorId: 23,
            userId: -1,
            // asdasd: "asd",
        } as Click & any);
        await app.close();
    });

});

