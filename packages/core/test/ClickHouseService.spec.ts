import {createAndResolve} from "./utils";
import {ClickHouseService} from "../src/ClickHouseService";

describe("ClickHouseService", async function () {
    this.timeout(Infinity);

    it("insert", async () => {
        let {app, service} = await createAndResolve(ClickHouseService);
        await service.insert("click", {});
        await app.close();
    });

});

