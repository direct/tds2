import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AffiliateNetwork} from "./entity/AffiliateNetwork";
import {User} from "./entity/User";
import {Source} from "./entity/Source";
import {Offer} from "./entity/Offer";
import {Campaign} from "./entity/Campaign";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: "mysql",
            synchronize: true,
            username: "local",
            password: "local",
            database: "local",
            // logging: true,
            port: 3506,
            entities: [
                AffiliateNetwork,
                Campaign,
                Offer,
                User,
                Source,
            ],
        }),
    ],
})
export class DatabaseModule {

}
