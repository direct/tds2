import Ajv, {JSONSchemaType} from "ajv";
import {Click} from "./Click";

export class ClickService {

    private schema: JSONSchemaType<Click, true> = {
        type: "object",
        properties: {
            visitorId: {type: "string", minLength: 3,},
            userId: {type: "number", minimum: 1},
        },
        required: ["userId"],
        additionalProperties: false,
    };

    private validate = (new Ajv({allErrors: true})).compile(this.schema)

    public async add(data: any) {
        let result = this.validate(data);
        console.log(result);
        // localize.ru(this.validate.errors);
        console.log(this.validate.errors);

    }
}


