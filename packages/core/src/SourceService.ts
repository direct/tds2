import {EntityManager} from "typeorm";
import {Injectable} from "@nestjs/common";
import {SourceAddParameters, SourceUpdateParameters} from "./Types";
import {User} from "./entity/User";
import {Source} from "./entity/Source";

@Injectable()
export class SourceService {

    constructor(
        private readonly entityManager: EntityManager,
    ) {
    }

    public async add(input: SourceAddParameters) {
        let user = await this.entityManager.findOneOrFail(User, {id: input.userId});
        let entity = this.entityManager.create(Source, {
            ...input,
            user,
        });
        return this.entityManager.save(entity);
    }

    public async update(id: number, input: SourceUpdateParameters) {
        let entity = await this.entityManager.findOneOrFail(Source, {id});
        this.entityManager.merge(Source, entity, input);
        return this.entityManager.save(entity);
    }
}