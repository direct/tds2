import requestPromise from "request-promise-native";

export class ClickHouseService {

    private http = requestPromise.defaults({
        baseUrl: "http://localhost:8123/",
    });

    async insert(into: string, clickData: Record<string, string>) {
        let data = Array.isArray(clickData)
            ? clickData.map(d => JSON.stringify(d)).join("\n")
            : JSON.stringify(clickData);
        let q = `INSERT INTO ${into} FORMAT JSONEachRow \n ${data}`;
        return await this.query(q);
    }

    private async query(query: string): Promise<string> {
        return this.http.post("/", {body: query});
    }

}