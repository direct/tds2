import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import {User} from "./User";
import {AffiliateNetwork} from "./AffiliateNetwork";
import {OfferType, RedirectTypes, ActionType} from "../Types";
import {Campaign} from "./Campaign";

@Entity()
export class Offer {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @CreateDateColumn()
    created!: Date;

    @ManyToOne(() => AffiliateNetwork, {nullable: true})
    affiliateNetwork?: AffiliateNetwork | null;

    @RelationId((entity: Offer) => entity.affiliateNetwork)
    affiliateNetworkId?: number | null;

    @ManyToOne(() => User, {nullable: false})
    user!: User;

    @RelationId((entity: Offer) => entity.user)
    userId!: number;

    @Column("enum", {enum: Object.keys(OfferType)})
    type!: keyof typeof OfferType;

    // ***** type redirect
    @Column("enum", {enum: Object.keys(RedirectTypes), nullable: true})
    redirectType?: keyof typeof RedirectTypes;

    @Column({nullable: true})
    redirectUrl?: string;

    // ***** type preload
    @Column({nullable: true})
    preloadUrl?: string;

    // ***** type action
    @Column("enum", {enum: Object.keys(ActionType), nullable: true})
    actionType?: keyof typeof ActionType;

    @ManyToOne(() => Campaign, {nullable: true})
    actionCampaign?: Campaign | null;

    @RelationId((entity: Offer) => entity.actionCampaign)
    actionCampaignId?: number;

    @Column("text", {nullable: true})
    actionContent?: string;
}
