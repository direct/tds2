import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import {User} from "./User";

@Entity()
export class AffiliateNetwork {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @Column("varchar", {nullable: true})
    public offerParams?: string | null;

    @Column("varchar", {nullable: true})
    public postbackUrl?: string | null;

    @CreateDateColumn()
    created!: Date;

    @ManyToOne(() => User, {nullable: false})
    user!: User;

    @RelationId((entity: AffiliateNetwork) => entity.user)
    userId!: number;

}
