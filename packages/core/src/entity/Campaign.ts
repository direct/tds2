import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import {User} from "./User";

@Entity()
export class Campaign {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @CreateDateColumn()
    created!: Date;

    @ManyToOne(() => User, {nullable: false})
    user!: User;

    @RelationId((entity: Campaign) => entity.user)
    userId!: number;

}
