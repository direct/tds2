import {Column, PrimaryColumn} from "typeorm";

export class Click {

    id!: string;
    visitorId!: string;
    userId!: number;
    created!: Date;
    campaignId!: number;
    previousCampaignId?: number;
    offerId?: number;
    affiliateNetworkId?: number;
    sourceId?: number;
    streamId!: number;
    uniqStream?: boolean;
    uniqCampaign?: boolean;
    uniqGlobal?: boolean;
    ip?: string;
    referer?: string;

    // итоговый урл куда будет отправлен пользователь
    location?: string;

    language?: string;
    deviceType?: string;
    userAgent?: string;
    os?: string;
    osVersion?: string;
    browser?: string;
    browserVersion?: string;
    deviceModel?: string;

    country?: string;
    region?: string;
    city?: string;

    connectionType?: string;
    operator?: string;

    // провайдер
    isp?: string;

}
