import {User} from "./entity/User";
import {AffiliateNetwork} from "./entity/AffiliateNetwork";
import {Source} from "./entity/Source";
import {Offer} from "./entity/Offer";

type Nullable<T> = {
    [P in keyof T]?: T[P] | null;
};

type AddParams<T, RKeys extends keyof T, PKeys extends keyof T> = Pick<Required<T>, RKeys> & Pick<Partial<T>, PKeys>
type UpdateParams<T, PKeys extends keyof T, NKeys extends keyof T> = Pick<Partial<T>, PKeys> & Pick<Nullable<T>, NKeys>

export type UserAddParameters = Pick<Required<User>, "email" | "password">

export type AffiliateNetworkAddParameters = AddParams<AffiliateNetwork, "name" | "userId", "postbackUrl" | "offerParams">
export type AffiliateNetworkUpdateParameters = UpdateParams<AffiliateNetwork, "name", "postbackUrl" | "offerParams">

export type SourceAddParameters = AddParams<Source, "name" | "userId", "postbackUrl">
export type SourceUpdateParameters = UpdateParams<Source, "name", "postbackUrl">

export type OfferAddParameters = AddParams<Offer, "name" | "userId" | "type", "affiliateNetworkId" | "redirectType" | "redirectUrl" | "preloadUrl" | "actionType" | "actionCampaignId" | "actionContent"> & {file?:Buffer}
export type OfferUpdateParameters = UpdateParams<Offer, "name", "affiliateNetworkId">

export const OfferType = {
    local: "Локальный",
    redirect: "Редирект",
    preload: "Предзагрузка",
    action: "Действие",
}


export const RedirectTypes = {
    http: "HTTP-редирект",
    meta: "Meta-редирект",
    js: "JS-редирект",
    emptyRef: "Редирект с пустым реферером",
    doubleMeta: "Двойной meta-редирект",
    formSubmit: "FormSubmit",
    iframe: "Открыть в iframe",
    remoteRedirect: "Remote-редирект",
}

export const ActionType = {
    goToCampaign: "Отправить в компанию",
    show404: "Показать 404 ошибку",
    text: "Показать текст",
    html: "Показать HTML",
    nothing: "Ничего не делать",
}


// type OfferTypeLocal = {
//     type: "local",
//     file: string;
// }
//
// type OfferTypeRedirect = {
//     type: "redirect",
//     redirectType: string,
//     redirectUrl: string,
// }
//
// type OfferTypePreload = {
//     type: "preload",
//     preloadUrl: string,
// }
//
// type OfferTypeAction = {
//     type: "action",
//     actionType: string,
// }
//
//
// type asdasd = OfferTypeLocal | OfferTypeRedirect | OfferTypeAction | OfferTypePreload;
//
// let asd: asdasd = {
//     type: "local",
//     // actionType: "sss",
//     file: ""
//     // redirectUrl: "asd",
// }

// let asd = keyof asdasd["type"]