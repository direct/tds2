import {Module} from "@nestjs/common";
import {UserService} from "./UserService";
import {DatabaseModule} from "./DatabaseModule";
import {AffiliateNetworkService} from "./AffiliateNetworkService";
import {SourceService} from "./SourceService";
import {OfferService} from "./OfferService";
import {ClickHouseService} from "./ClickHouseService";
import {ClickService} from "./ClickService";

@Module({
    imports: [
        DatabaseModule,
    ],
    providers: [
        AffiliateNetworkService,
        ClickHouseService,
        ClickService,
        UserService,
        SourceService,
        OfferService,
    ],
})
export class AppModule {


}