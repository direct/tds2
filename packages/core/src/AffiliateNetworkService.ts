import {AffiliateNetwork} from "./entity/AffiliateNetwork";
import {EntityManager} from "typeorm";
import {Injectable} from "@nestjs/common";
import {AffiliateNetworkAddParameters, AffiliateNetworkUpdateParameters} from "./Types";
import {User} from "./entity/User";

@Injectable()
export class AffiliateNetworkService {

    constructor(
        private readonly entityManager: EntityManager,
    ) {
    }

    public async add(input: AffiliateNetworkAddParameters) {
        let user = await this.entityManager.findOneOrFail(User, {id: input.userId});
        let entity = this.entityManager.create(AffiliateNetwork, {
            ...input,
            user,
        });
        return this.entityManager.save(entity);
    }

    public async update(id: number, input: AffiliateNetworkUpdateParameters) {
        let entity = await this.entityManager.findOneOrFail(AffiliateNetwork, {id});
        this.entityManager.merge(AffiliateNetwork, entity, input);
        return this.entityManager.save(entity);

    }
}