import {EntityManager} from "typeorm";
import {Injectable, Type} from "@nestjs/common";
import {OfferAddParameters, OfferUpdateParameters} from "./Types";
import {Offer} from "./entity/Offer";
import {User} from "./entity/User";
import {AffiliateNetwork} from "./entity/AffiliateNetwork";
import {Campaign} from "./entity/Campaign";
import {EntityTarget} from "typeorm/common/EntityTarget";

@Injectable()
export class OfferService {

    constructor(
        private readonly entityManager: EntityManager,
    ) {
    }

    public async add(input: OfferAddParameters) {
        let user = await this.entityManager.findOneOrFail(User, {id: input.userId});
        let affiliateNetwork = await this.getEntityById(AffiliateNetwork, input.affiliateNetworkId);
        let actionCampaign = await this.getEntityById(Campaign, input.actionCampaignId);
        let entity = this.entityManager.create(Offer, {
            ...input,
            affiliateNetwork,
            user,
            actionCampaign,
        });
        return this.entityManager.save(entity);
    }

    public async update(id: number, input: OfferUpdateParameters) {
        let entity = await this.entityManager.findOneOrFail(Offer, {id});
        this.entityManager.merge(Offer, entity, input);
        return this.entityManager.save(entity);

    }

    private async getEntityById<T>(type: EntityTarget<T>, id: number | undefined | null): Promise<T | undefined | null> {
        if (id === null) {
            return null;
        }
        if (typeof id === "undefined") {
            return undefined;
        }
        return this.entityManager.findOneOrFail(type, {id} as any);
    }
}