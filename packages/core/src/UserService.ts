import {UserAddParameters} from "./Types";
import {EntityManager} from "typeorm";
import {User} from "./entity/User";
import {Injectable} from "@nestjs/common";

@Injectable()
export class UserService {

    constructor(
        private readonly entityManager: EntityManager,
    ) {
    }

    public add(input: UserAddParameters): Promise<User> {
        let user = this.entityManager.create(User, input);
        return this.entityManager.save(user);
    }

}